PACKAGE = "hachoir"
from hachoir import __version__ as VERSION
WEBSITE = 'http://hachoir.readthedocs.io/'
LICENSE = 'GNU GPL v2'
