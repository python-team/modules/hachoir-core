Source: hachoir
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Build-Depends: debhelper-compat (= 13), dh-python
Build-Depends-Indep: python3-all, python3-setuptools, python3-urwid, python3-wxgtk4.0
Standards-Version: 4.6.1
Homepage: https://github.com/vstinner/hachoir
Vcs-Git: https://salsa.debian.org/python-team/packages/hachoir.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/hachoir

Package: hachoir
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
 python3-urwid,
 python3-wxgtk4.0
Provides: python3-hachoir
Breaks:
 python-hachoir-core,
 python-hachoir-metadata,
 python-hachoir-parser,
 python-hachoir-regex,
 python-hachoir-subfile,
 python-hachoir-urwid,
 python-hachoir-wx
Replaces:
 python-hachoir-core,
 python-hachoir-metadata,
 python-hachoir-parser,
 python-hachoir-regex,
 python-hachoir-subfile,
 python-hachoir-urwid,
 python-hachoir-wx
Description: Python framework to parse and edit binary files
 Hachoir is a Python library used to represent of a binary file as a tree of
 Python objects. Each object has a type, a value, an address, etc. The goal is
 to be able to know the meaning of each bit in a file.
 .
 Why using slow Python code instead of fast hardcoded C code? Hachoir has many
 interesting features:
  * Autofix: Hachoir is able to open invalid / truncated files
  * Lazy: Open a file is very fast since no information is read from file,
    data are read and/or computed when the user ask for it
  * Types: Hachoir has many predefined field types (integer, bit, string, etc.)
    and supports string with charset (ISO-8859-1, UTF-8, UTF-16, ...)
  * Addresses and sizes are stored in bit, so flags are stored as classic fields
  * Endian: You have to set endian once, and then number are converted in the
    right endian
  * Editor: Using Hachoir representation of data, you can edit, insert, remove
    data and then save in a new file.
 .
 Hachoir provides command-line tools using its parsers:
  * hachoir-grep: find a text pattern in a binary file
  * hachoir-metadata: get metadata from binary files
  * hachoir-strip: modify a file to remove metadata
  * hachoir-urwid: display the content of a binary file in text mode
  * hachoir-wx: display the content of a binary file in a GUI
